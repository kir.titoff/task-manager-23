package ru.t1.ktitov.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all projects";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final String userId = getUserId();
        getProjectService().clear(userId);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
